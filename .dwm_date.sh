#!/bin/sh

function now {
	date "+ 📅 %a %b %d %Y 🕑 %-l:%M %p %Z"
}

# Battery
battery_prefix=/sys/class/power_supply/BAT1

function capacity {
	cat $battery_prefix/capacity
}

function warn_battery {
	if [ -e $battery_prefix -a $(capacity) -lt 10 \
			-a $(< $battery_prefix/status) = Discharging ]; then
		printf 'Check battery! %.0s' 1 2 3 4 5 | \
		dmenu -fn 'UbuntuMono:size=16' -sb '#9c1f16' \
		> /dev/null
	fi
}

function status {
	case $(< $battery_prefix/status) in
		Discharging)	echo ↓ ;;
		Charging)	echo ↑ ;;
		Full)		echo ✓ ;;
	esac
}

if [[ -e $battery_prefix ]]; then
	function battery { echo " 🔋$(status)$(capacity)%"; }
else
	function battery { echo ""; }
fi

# Main
function status_bar {
	xsetroot -name "$(battery)$(now)"
}

while true; do
	status_bar
	warn_battery
	sleep $(expr 60 - $(date +%S))
done
