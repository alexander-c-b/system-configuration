-- Replace the more readable '{...}/{...}' with LaTeX '\frac{...}{...}' in math
-- elements.


-- Replace '{string1}/{string2}' with
-- '\frac{' .. replace (string1) .. '}{' .. replace(string2) .. '}'.
local function replace(s)
    return s:gsub('(%b{})/(%b{})', function (numer, denom)
        return '\\frac' .. replace(numer) .. replace(denom)
    end)
end

return {
    {
        Math = function(el)
            return pandoc.Math(el.mathtype, replace(el.text))
        end
    }
}
