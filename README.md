# Configuration files
This is a repository of the many configuration files I use on my [NixOS](https://nixos.org/) Linux computers.

## [`.config/`](./.config/)
-   [`z-nix-config/`](.config/z-nix-config) contains all my NixOS configuration; principally, [`system.nix`](.config/z-nix-config/system.nix) contains the main configuration, while [`laptop.nix`](.config/z-nix-config/laptop.nix), [`desktop.nix`](.config/z-nix-config/desktop.nix) and [`hp-spectre.nix`](.config/z-nix-config/hp-spectre.nix)  contain specific configuration for different computers.  [`ghcPackages.nix`](.config/z-nix-config/ghcPackages.nix) provides [Pandoc](https://pandoc.org/index.html) and necessary utilities for document creation.  [`packages/package-lists.nix`](.config/z-nix-config/packages/package-lists.nix) enumerates the main packages I use.  [`packages/`](.config/z-nix-config/packages) also contains several custom packages I use.

-   [`nvim/`](.config/nvim) contains my configuration for my text editor of choice, [Neovim](https://neovim.io/).  [`init.vim`](.config/nvim/init.vim) is the most important; I also have many [snippet](https://en.wikipedia.org/wiki/Snippet_(programming)) files under `UltiSnips`.  I have a customized colorscheme under [`colors/alexander-eink.vim`](.config/nvim/colors/alexander-eink.vim), based on [Kyle Isom's `eink.vim`](https://en.wikipedia.org/wiki/Snippet_(programming))

-   [`qutebrowser`](.config/qutebrowser) houses my customizations to the minimal [Qutebrowser](https://qutebrowser.org/).  Qutebrowser allows me to browse almost entirely without a mouse; it uses keybindings similar to those of [Neovim](https://neovim).  [`config.py`](.config/qutebrowser/config.py) is the main location of my configurations.

-   [`dwm/`](.config/dwm) contains my custom configuration for the [Dynamic Window Manager (DWM)](https://dwm.suckless.org/).  [`config.h`](.config/dwm/config.h) contains the customizations.  [`overrides.nix`](.config/dwm/overrides.nix) enumerates the "patches" which extend DWM's functionality.

## [`scripts/`](./scripts/)
This directory includes many miscellaneous tools and scripts.  The most important are [`mdtopdf`](scripts/mdtopdf), [`scroll.sh`](scripts/scroll.sh) and [`nvimd`](scripts/nvimd).

I also use [`to-clipboard`](scripts/to-clipboard) and [`from-clipboard`](scripts/from-clipboard) quite often, which are thin wrappers over [`xclip`](https://github.com/astrand/xclip).

## [`.local/share/`](./.local/share/)
[`pandoc/templates/default.latex`](.local/share/pandoc/templates/default.latex) is my fork of the default [ConTeXt](https://wiki.contextgarden.net/Main_Page) template from [Pandoc](https://pandoc.org/index.html), the document conversion tool underpinning my process for writing professional documents in Markdown.
