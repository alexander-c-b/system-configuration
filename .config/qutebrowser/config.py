# Page Up is "<PgUp>", Page Down is "<PgDown>", End is "<End>"
config.load_autoconfig()

c.editor.command = ["st", "-e",
                    "nvim",
                    "-f", "{file}",
                    "-c", "set filetype=markdown",
                    "-c", "normal {line}G{column0}l"]

config.bind("<Ctrl-N>", "completion-item-focus next", mode="command")
config.bind("<Ctrl-P>", "completion-item-focus prev", mode="command")
config.bind("<Ctrl-O>", "back")
config.bind("<Ctrl-I>", "forward")
config.bind
config.unbind("<Shift-H>")
config.unbind("<Shift-L>")
config.bind("<Ctrl-[>", "leave-mode",  mode="passthrough")
config.bind("<Ctrl-E>", "open-editor", mode="passthrough")
config.bind("\\s", "spawn --userscript switch_page_up_down.sh")
config.bind("\\p", "spawn --userscript html-to-pdf.sh")

pass_cmd = "spawn --userscript qute-pass --dmenu-invocation dmenu"
config.bind("zl",  pass_cmd)
config.bind("zul", pass_cmd + " --username-only")
config.bind("zpl", pass_cmd + " --password-only")

for mode in ("insert", "passthrough"):
    config.bind("<Ctrl-W>", "fake-key <Ctrl-Backspace>", mode=mode)
    config.bind("<Ctrl-H>", "fake-key <Backspace>",      mode=mode)
    config.bind("<Ctrl-M>", "fake-key <Enter>",          mode=mode)
    config.bind("<Ctrl-I>", "fake-key <Tab>",            mode=mode)
config.bind(";u", "hint url run open    {hint-url}")
config.bind(";U", "hint url run open -t {hint-url}")
config.bind(";c", "hint all right-click")
config.bind(";p", "hint url spawn --userscript pdf {hint-url}")
config.bind(";r", "hint --rapid all")
config.bind(";R", "hint --rapid links tab-bg")
config.bind("<Ctrl-P>", "prompt-open-download odownloadpdf", mode="prompt")
for mode in ["normal", "caret"]:
    config.bind(";si", "spawn --userscript translate.sh gle", mode=mode)
c.hints.chars = "qwerasdxcv"
c.hints.selectors["calendar"] = ["div[data-bubble-scrollable-root]"]

c.url.searchengines = {"DEFAULT": "https://duckduckgo.com/?q={}",
                       "fr":      "https://www.google.fr/search?q={}"}

c.fonts.web.family.sans_serif   = "Ubuntu"
c.fonts.web.family.standard     = "Ubuntu"
c.fonts.web.family.fixed        = "Ubuntu Mono"
c.fonts.completion.entry        = "Ubuntu"
c.fonts.tabs.selected           = "Ubuntu"
c.fonts.tabs.unselected         = "Ubuntu"
c.fonts.keyhint                 = "Ubuntu"
c.fonts.downloads               = "Ubuntu"
c.fonts.debug_console           = "Ubuntu"

# DWM status bar colors
c.colors.tabs.selected.odd.bg         = "#222222"
c.colors.tabs.selected.even.bg        = "#222222"
c.colors.tabs.pinned.selected.odd.bg  = "#222222"
c.colors.tabs.pinned.selected.even.bg = "#222222"

c.content.user_stylesheets = str(config.configdir / "default.css")

# Site-specific settings

## Florida Poly CAMS
with config.pattern('https://cams.floridapoly.org/*') as p:
    p.content.javascript.can_open_tabs_automatically = True
