{ config, pkgs, options, ... }:

{
  # Boot loader
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint     = "/boot";
    };
    systemd-boot.enable = false;
    grub = {
      enable     = true;
      efiSupport = true;
      device     = "nodev";
      extraEntries = ''
        menuentry "Windows 11" {
          chainloader /EFI/Microsoft/Boot/bootmgfw.efi
        }
        menuentry 'UEFI Firmware Settings' --id 'uefi-firmware' {
          fwsetup
        }
      '';
    };
  };
  boot.kernelParams = [ "nohibernate" ];

  # Hibernation
  # hibernate instead of sleep (also preserves wifi)
  systemd.sleep.extraConfig = ''
    [Sleep]
    AllowSuspend=no
    AllowHibernation=no
  '';

  services.upower = {
    enable = true;
    criticalPowerAction = "PowerOff";
  };

  # Networking
  networking = {
    networkmanager.enable      = true;
    useDHCP                    = false;
    interfaces.enp60s0.useDHCP = true;
    interfaces.wlo1.useDHCP    = true;
    hostName                   = "z-nixos-msi-laptop";
    hostId                     = "04af080a";
  };

  # Global keybindings
  services.actkbd = {
    enable = true;

    bindings = let light = "/run/current-system/sw/bin/light"; in [
      # Brightness up
      { keys = [ 225 ]; events = [ "key" ]; command = "${light} -A 5"; }
      # Brightness down
      { keys = [ 224 ]; events = [ "key" ]; command = "${light} -U 5"; }
    ];
  };

  services.xserver = {
    # Enable touchpad support.
    libinput.enable = true;
    libinput.touchpad = {
      tapping          = true;
      naturalScrolling = true;
      scrollMethod     = "twofinger";
    };
    displayManager.autoLogin = {
      enable = true;
      user   = "alexander";
    };
  };

  # Video Drivers
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.prime = {
    offload.enable =
      if options.hardware.nvidia.prime.sync.enable.value
      then false
      else true;
    intelBusId     = "PCI:0:2:0";
    nvidiaBusId    = "PCI:1:0:0";
  };

  specialisation = {
    docked.configuration = {
      hardware.nvidia.prime.sync.enable = true;
      networking.networkmanager.unmanaged = [ "wlo1" ];
    };
  };


  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?
}
