# vim: set tabstop=8 softtabstop=2 shiftwidth=2 expandtab number relativenumber:
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  homePath = "/home/alexander";
  thisSystem = ./. + "/${builtins.readFile /etc/nixos/this-system}.nix";
in rec {
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
      thisSystem
    ];

  nix = {
    autoOptimiseStore = true;
    useSandbox        = true;
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
    '';
  };
  environment.pathsToLink = [ "/share/nix-direnv" ];

  nixpkgs.config.allowUnfree = true;

  # Time zone.
  time.timeZone = "America/New_York";

  # OpenSSH
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
  };

  # CUPS printing
  services.printing = {
    enable  = true;
    drivers = [ pkgs.epson-escpr ];
  };

  # Sound
  sound.enable = true;
  hardware.pulseaudio.enable  = true;

  # Bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable   = true;

  # Virtualization
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "alexander" ];

  # Application Config
  nixpkgs.config = {
    # Override dwm with post patch to use my config file.
    packageOverrides = super: let self = super.pkgs; in {
      dwm = super.dwm.overrideAttrs (import ../dwm/overrides.nix);

      dmenu = super.dmenu.overrideAttrs (oldAttrs: rec {
        patches = let
          fuzzymatch = builtins.fetchurl {
            url    = "https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff";
            sha256 = "0yababzi655mhpgixzgbca2hjckj16ykzj626zy4i0sirmcyg8fr";
          };
          fuzzyhighlight = builtins.fetchurl {
            url    = "https://tools.suckless.org/dmenu/patches/fuzzyhighlight/dmenu-fuzzyhighlight-4.9.diff";
            sha256 = "0mxa4ycv0s3qwy3lrsg2znzh28waisjrh6snyrqbc7vjd8dcrjw2";
          };
        in [ fuzzymatch fuzzyhighlight ];
        postPatch = ''
          sed -i 's/if (\*highlight == text[i]) {/if (!fstrncmp(&(*highlight), &text[i], 1)) {/' \
            dmenu.c
          sed -i 's/monospace:size=10/UbuntuMono:size=16/' config.def.h
        '' + oldAttrs.postPatch;
      });
    };
  };

  qt5.platformTheme = "gtk";
  programs = {
    zsh = {
      enable = true;
      enableCompletion = true;
    };
    slock.enable      = true;
    git.enable        = true;
    git.lfs.enable    = true;
  };

  # System Packages
  environment.systemPackages = with import ./package-lists.nix pkgs;
    system ++ basic ++ mediaProcessing ++ graphical;
  services.emacs.enable = true;

  # Fonts
  fonts.fonts = with pkgs; [
    ubuntu_font_family
    lmodern
    xits-math
    (pkgs.callPackage ./fonts/times.nix { })
    (pkgs.callPackage ./fonts/fira-math.nix { })
  ];
  fonts.fontDir.enable = true;
  fonts.fontconfig.enable = true;

  # Enable the X11 windowing system.
  services.xserver = {
    enable       = true;
    wacom.enable = true;
    autorun      = true;
    layout       = "us";
    xkbOptions   = "eurosign:e";
    displayManager.xserverArgs  = [ "-ardelay 200" ];
    desktopManager.xterm.enable = true;
    windowManager.dwm.enable    = true;
  };

  services.redshift = {
    enable = true;
    temperature.night = 3500;
  };
  location.latitude  =  27.8961111;
  location.longitude = -81.8433333;

  # Users
  users.extraGroups.nixosconf = {
    members = [ "alexander" "root" ];
  };

  users.users.alexander = {
    isNormalUser = true;
    home         = homePath;
    # "wheel": Enable 'sudo' for the user.
    extraGroups = [
      "wheel"
      "networkmanager"
      "video"
      "audio"
      "scanner"
      "lp"
      "alexander"
      "nixosconf"
    ];
    shell = pkgs.zsh;
    uid   = 1000;
  };

  environment.shells = [ pkgs.zsh ];
  users.defaultUserShell = pkgs.zsh;

  # Filesystems
  fileSystems = {
    "/mnt/file-server" = {
      device     = "//ubuntu-server/users";
      mountPoint = "/mnt/file-server";
      fsType     = "cifs";
      options    = [
        "_netdev" "x-systemd.mount-timeout=30" "user" "uid=1000" "gid=1000"
        "nofail" "auto" "credentials=${homePath}/.server-credentials"
      ];
    };

    "/mnt/alexander-file-server" = {
      device     = "//ubuntu-server/zander";
      mountPoint = "/mnt/alexander-file-server";
      fsType     = "cifs";
      options    = [
        "_netdev" "x-systemd.mount-timeout=30" "user" "uid=1000" "gid=1000"
        "nofail" "auto" "credentials=${homePath}/.server-credentials"
      ];
    };
  };

  boot.supportedFilesystems = [ "zfs" ];
  services.zfs.autoScrub = {
    enable   = true;
    interval = "Sunday, 12:00";
  };

  networking.hosts = {
    "192.168.1.3" = [ "ubuntu-server" ];
  };

}

