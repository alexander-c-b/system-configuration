{ config, pkgs, ... }:

{
  # Boot loader
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint     = "/boot/";
    };
    systemd-boot.enable = true;
  };

  services.xserver.videoDrivers = [ "nvidia" ];

  networking = {
    useDHCP                   = false;
    interfaces.eno1.useDHCP   = true;
    interfaces.wlp4s0.useDHCP = true;
    networkmanager.enable     = true;
    hostName                  = "z-nixos-desktop";
    hostId                    = "156CA968";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?
}
