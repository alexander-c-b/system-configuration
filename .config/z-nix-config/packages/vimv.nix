{ stdenv, lib, fetchFromGitHub }:

stdenv.mkDerivation {
  name = "vimv";

  src  = fetchFromGitHub {
    owner  = "thameera";
    repo   = "vimv";
    rev    = "3bb51a43b75a8b0166e4e1dfe86776a9b2a131c9";
    sha256 = "1iz30j3n006dv7rllzgzxil0fs918wqxj9frpf7mzkj87zkd3y0s";
  };

  makeFlags = [ "PREFIX=$(out)" ];

  meta = {
    homepage = "https://github.com/thameera/vimv";
    description = ''
      vimv is a terminal-based file rename utility that lets you easily
      mass-rename files using Vim.
    '';
    license   = lib.licenses.mit;
    platforms = lib.platforms.linux;
  };
}
