{ stdenv, lib, ... }:

stdenv.mkDerivation rec {
  name    = "istyle-verilog-formatter";
  version = "1.23";

  src = builtins.fetchTarball {
    url    = "https://github.com/thomasrussellmurphy/istyle-verilog-formatter/archive/refs/tags/v${version}.tar.gz";
    sha256 = "0gscnq0qi0c39s6fy95b911s13f8svlj72cvfwapmbdnf6ylskac";
  };

  installPhase = ''
    mkdir -p $out/bin
    mv bin/release/iStyle $out/bin
  '';

  meta = {
    description =
      "A fast and free automatic formatter for Verilog source code";
    homepage    =
      "https://github.com/thomasrussellmurphy/istyle-verilog-formatter";
    platforms   = lib.platforms.linux;
    license     = lib.licenses.gpl2;
  };
}
