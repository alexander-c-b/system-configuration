" Plugins {{{
" Setup {{{
let s:plugged_path = stdpath('data').'/site/autoload/plug.vim'
if empty(s:plugged_path)
    execute 'silent !curl -fLo '.s:plugged_path.' --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let s:plugin_dir = stdpath('data') . "/site/plugged"
call mkdir(s:plugin_dir, "p")
call plug#begin(s:plugin_dir)
" }}}

" Style {{{
Plug 'itchyny/lightline.vim',           {'commit': 'b06d921023cf6536bcbee5754071d122296e8942'}
" }}}

" Miscellanious Utility {{{
Plug 'lifepillar/vim-mucomplete',       {'commit': '83cd9b3775438faafc3475f9f9d5fbb8da4dfa5b'}
Plug 'neovim/nvim-lspconfig',           {'commit': '01153c52b3a2c9a27f7c953f3262131aac5a7277'}
Plug 'SirVer/ultisnips',                {'commit': 'b2902a65311e0f1ef18c675fa63f3a5ccb4a0b47'}
Plug 'jiangmiao/auto-pairs',            {'commit': '39f06b873a8449af8ff6a3eee716d3da14d63a76'}
Plug 'tpope/vim-surround',              {'commit': 'f51a26d3710629d031806305b6c8727189cd1935'}
Plug 'tpope/vim-repeat',                {'commit': '24afe922e6a05891756ecf331f39a1f6743d3d5a'}
Plug 'tpope/vim-abolish',               {'commit': '3f0c8faadf0c5b68bcf40785c1c42e3731bfa522'}
Plug 'vim-autoformat/vim-autoformat',   {'commit': 'bb11f30377985e45e2eecef570856d42dbabb8b0'}
Plug 'preservim/tagbar',                {'commit': '2da3443f5fb78aa0b9a60bb5b2926d72df734e14'}
Plug 'godlygeek/tabular',               {'commit': '339091ac4dd1f17e225fe7d57b48aff55f99b23a'}
Plug 'mbbill/undotree',                 {'commit': '1cc3b9069e4356efd4ce1c3c4bdbb227fb54e1e5'}
Plug 'kana/vim-textobj-user',           {'commit': '41a675ddbeefd6a93664a4dc52f302fe3086a933'}
Plug 'kana/vim-textobj-line',           {'commit': '0a78169a33c7ea7718b9fa0fad63c11c04727291'}
Plug 'christoomey/vim-titlecase',       {'commit': '630089d75ab7f0597ab6b3b4c4c3e2e011a05e64'}
Plug 'skywind3000/asyncrun.vim',        {'commit': '0ca4baec21998083220c6030afba5e116e3be9c4'}
Plug '907th/vim-auto-save',             {'commit': '8c1d5dc919030aa712ad7201074ffb60961e9dda'}
Plug 'JuliaEditorSupport/julia-vim',    {'commit': 'c3c35958e56993337d6517cf5d6e974aca1da99f'}
Plug 'editorconfig/editorconfig-vim',   {'commit': '3078cd10b28904e57d878c0d0dab42aa0a9fdc89'}
Plug 'Yggdroot/indentLine',             {'commit': '5617a1cf7d315e6e6f84d825c85e3b669d220bfa', 'on': 'IndentLinesToggle'}
" }}}

" Languages {{{
Plug 'plasticboy/vim-markdown',         {'for': 'markdown',     'commit': '8e5d86f7b85234d3d1b4207dceebc43a768ed5d4'}
Plug 'vim-scripts/indentpython.vim',    {'for': 'python',       'commit': '6aaddfde21fe9e7acbe448b92b3cbb67f2fe1fc1'}
Plug 'nvie/vim-flake8',                 {'for': 'python',       'commit': '719cc31e2d07021906cc6471b7d7b1863d2b6acf'}
Plug 'tmhedberg/SimpylFold',            {'for': 'python',       'commit': '0459df8a0bbfc8ef1bfd88db889e881626f65914'}
Plug 'dhruvasagar/vim-table-mode',      {'for': 'markdown',     'commit': '35e9fbf64c06fddc41651e65b92200f902d8ae0b'}
Plug 'LnL7/vim-nix',                    {'for': 'nix',          'commit': '63b47b39c8d481ebca3092822ca8972e08df769b'}
Plug 'elzr/vim-json',                   {'for': 'json',         'commit': '3727f089410e23ae113be6222e8a08dd2613ecf2'}
Plug 'vhda/verilog_systemverilog.vim',  {'for': 'verilog_systemverilog', 'commit': '0b88f2ccf81983944bf00d15ec810dd807053d19'}
" }}}

call plug#end()
" }}}

" Saving and Undo {{{
let g:auto_save = 1
let &undodir = stdpath('data').'/undo'
if !isdirectory(&undodir)
    call mkdir(&undodir, "p", 0700)
endif
set undofile
" }}}

" General Settings {{{
autocmd!
set tags+=./.tags;/
set completeopt=menu,menuone,preview,noinsert,noselect
set number relativenumber
set nohlsearch
set ignorecase smartcase
set splitbelow splitright
set scrolloff=2 sidescrolloff=5
set shiftround
set title
set wildmenu path+=**       " File movement
set exrc
filetype plugin indent on
filetype plugin on
syntax on
let loaded_matchit = 1 " Don't load 'matchit' plugin
set foldlevelstart=99
" }}}

" Constants {{{
let g:pi = 3.14159
" }}}

" Style {{{
set background=light
colorscheme alexander-eink
set laststatus=2  " Always show status line
set t_Co=256      " number of terminal colors
set noshowmode    " have the status line rather than vim show the mode
" Highlighting {{{
highlight clear SpellCap
highlight clear SignColumn
highlight link trailing_whitespace InlineCode
" Text-Width Column Highlighting {{{
autocmd Filetype * let &l:colorcolumn = (&textwidth ? &textwidth : 79)  + 1
autocmd OptionSet textwidth let &l:colorcolumn = (&textwidth ? &textwidth : 79)  + 1
" }}}
" }}}
" }}}

" Utility Plugins {{{
" Note that plugin-related key mappings are also contained here
" " MUcomplete {{{
let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#completion_delay       = 200
let g:mucomplete#cycle_with_trigger     = 1
let g:mucomplete#no_mappings            = 1
imap <C-B> <plug>(MUcompleteFwd)
imap <C-V> <plug>(MUcompleteBwd)
" Always insert new line on <Enter>
inoremap <expr> <CR> pumvisible() ? "<C-Y><CR>" : "<CR>"
let s:mu_text_chain = ['keyp', 'keyn', 'c-p', 'c-n', 'path', 'ulti', 'uspl', 'dict']
let g:mucomplete#chains = {
    \ 'default':   ['keyp', 'keyn', 'c-p', 'c-n', 'omni', 'path'],
    \ 'cpp':       ['omni', 'path'],
    \ 'python':    ['omni', 'path'],
    \ 'pmarkdown': s:mu_text_chain,
    \ 'markdown':  s:mu_text_chain,
    \ 'text':      s:mu_text_chain,
    \ }
let s:cpp_cond = { t -> t =~# '\%(\a\|\.\|->\|::\)$' }
let g:mucomplete#can_complete = {}
let g:mucomplete#can_complete.cpp     = { 'omni': s:cpp_cond }
" }}}
" " Neovim LSP {{{
lua << EOF
local custom_lsp_attach = function(client)
  -- See `:help nvim_buf_set_keymap()` for more information
  vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', {noremap = true})
  vim.api.nvim_buf_set_keymap(0, 'n', '<c-]>', '<cmd>lua vim.lsp.buf.definition()<CR>', {noremap = true})

  -- For plugins with an `on_attach` callback, call them here. For example:
  -- require('completion').on_attach()
end

require('lspconfig').pylsp.setup{ on_attach = custom_lsp_attach }
require('lspconfig').ccls.setup{ on_attach = custom_lsp_attach }
EOF
autocmd Filetype cpp,py set omnifunc=v:lua.vim.lsp.omnifunc
" }}}
" " Ultisnips {{{
let g:UltiSnipsSnippetDirectories   = [stdpath('config').'/UltiSnips']
let g:UltiSnipsExpandTrigger        = "<C-L>"
let g:UltiSnipsJumpForwardTrigger   = "<C-J>"
let g:UltiSnipsJumpBackwardTrigger  = "<C-G>b"
let g:UltiSnipsEditSplit            = "horizontal"
inoremap <C-J> <Nop>
" }}}
" " Auto Pairs {{{
autocmd Filetype pmarkdown,markdown let b:AutoPairs =
    \ {'(':')', '[':']', '{':'}', '\\langle':'\rangle', "'":"'", '"':'"', "`":"`",
    \  '\V\C\\left(' :'\right)', '\V\C\\left[' :'\right]', '\V\C\\left{' :'\right}',
    \  '\V\C\\bigl(' :'\bigr)',  '\V\C\\bigl[' :'\bigr]',  '\V\C\\bigl{' :'\bigr}',
    \  '\V\C\\Bigl(' :'\Bigr)',  '\V\C\\Bigl[' :'\Bigr]',  '\V\C\\Bigl{' :'\Bigr}',
    \  '\V\C\\biggl(':'\biggr)', '\V\C\\biggl[':'\biggr]', '\V\C\\biggl{':'\biggr}',
    \  '\V\C\\Biggl(':'\Biggr)', '\V\C\\Biggl[':'\Biggr]', '\V\C\\Biggl{':'\Biggr}',
    \  '\V\C\\left\\langle' :' \right\rangle', '\V\C\\bigl\\langle' :' \bigr\rangle',
    \  '\V\C\\Bigl\\langle' :' \Bigr\rangle',  '\V\C\\biggl\\langle':' \biggr\rangle',
    \  '\V\C\\Biggl\\langle':' \Biggr\rangle',
    \  '```':'```', '"""':'"""', "'''":"'''",
    \  '$':'$', '$$':'$$'}
autocmd Filetype nix let b:AutoPairs =
    \ {'(':')', '[':']', '{':'}',"'":"'",'"':'"',"''":"''"}
autocmd Filetype coq let b:AutoPairs =
    \ {'(':')', '[':']', '{':'}',"'":"'",'"':'"','(*':'*)'}
autocmd Filetype c,cpp,cuda let b:AutoPairs =
    \ {'(':')', '[':']', '{':'}',"'":"'",'"':'"','/*':'*/'}
autocmd Filetype verilog_systemverilog let b:AutoPairs =
    \ {'(':')', '[':']', '{':'}', '"':'"'}
" }}}
" " Julia Vim LaTeX-to-Unicode {{{
let g:latex_to_unicode_file_types = ".*"
" }}}
" " Vim-Markdown {{{
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_no_default_key_mappings = 1
" }}}
" " Autoformat {{{
" g:markdown_format: used for formatting Markdown files with Pandoc. If the
" environment variable $MARKDOWN_FORMAT is set, use that; if the current file
" matches one created by my nvimd script, use Pandoc Markdown; otherwise, use
" commonmark (like GitHub).
let g:markdown_format = !empty($MARKDOWN_FORMAT) ? $MARKDOWN_FORMAT
                        \ : bufname('%') =~# '^/tmp/nvimd-' ? "markdown"
                        \ : "commonmark"
let g:formatdef_pandoc = '"pandoc --standalone --columns=79
    \ -f ".g:markdown_format." -t ".g:markdown_format."
    \ --tab-stop=".&softtabstop'
let g:formatters_markdown = ['pandoc']

noremap <C-P> :Autoformat<CR>
" }}}
" }}}

" Functions {{{
" " General {{{
function! OpenPdf() abort
    silent !clear
    execute '!opdf "' . expand('%:h') . '.pdf"'
endfunction

function! TabularizeOp(mode) abort
    execute "'[,']Tabularize /" . input("Tabularize: ")
endfunction

function! TabularizeOpNoRightSpace(mode) abort
    execute "'[,']Tabularize /" . input("Tabularize: ") . "/l1r0"
endfunction

function! SetConcealLevel() abort
    if (&conceallevel ==# 0)
        set conceallevel=2
    else
        set conceallevel=0
    endif
endfunction
nnoremap <leader>l :call SetConcealLevel()<CR>

let s:WhiteSpaceHighlight = 0
function! ToggleWhiteSpaceHl() abort
    if s:WhiteSpaceHighlight == 0
        let s:WhiteSpaceHighlight = 1
        match trailing_whitespace :\s\+\%#\@<!$:
    else
        let s:WhiteSpaceHighlight = 0
        call clearmatches()
    endif
endfunction
call ToggleWhiteSpaceHl()
" }}}

" " C {{{
function! CCompileCurrentFile() abort
    silent !clear
    execute '!' . g:c_compiler . ' "' . bufname('%')
        \ . '" -O0 -Wall -Wno-unused-result -lm -g -U_FORTIFY_SOURCE -o "'
        \ . expand('%:h') . '"'
endfunction

function! CRunCurrentFile() abort
    silent !clear
    execute '!"./' . expand('%:h') . '"'
endfunction
" " }}}

" " Python {{{
function! PythonRunCurrentFile() abort
    silent !clear
    execute '!' . g:python_interpreter . ' "' . bufname('%') . '"'
endfunction
" " }}}

" " Markdown {{{
function! MarkdownToPdf() abort
    silent !clear
    write
    execute 'AsyncRun mdtopdf "'.bufname('%').'" "'.expand('%:h').'.pdf"'
endfunction

function! MarkdownCopyRichTextBuffer() abort
    silent !clear
    " The `<title>` element is problematic for GMail, so `sed` removes it.
    write !mdtopdf - -
        \ --from markdown+lists_without_preceding_blankline
        \ --to html --self-contained --webtex
        \ | sed 's:<title>-</title>::'
        \ | to-clipboard text/html
endfunction

function! MarkdownConvertClipboard() abort
    silent !clear
    !xclip -selection clipboard -target text/html -out
    \   | pandoc --from html-native_divs-native_spans
    \     --to markdown-auto_identifiers-header_attributes
    \     --wrap=none --atx-headers
    \   | xclip -in -selection clipboard
endfunction
" " }}}
" " }}}

" Keybindings {{{
" " Leader and Local Leader {{{
let mapleader      = ","
let maplocalleader = "\\"
nnoremap , <Nop>
" " }}}
" " Operators{{{
nmap     <leader>a <Plug>Titlecase
vmap     <leader>a <Plug>Titlecase
nnoremap <leader>tb :set operatorfunc=TabularizeOp<CR>g@
nnoremap <leader>tB :set operatorfunc=TabularizeOpNoRightSpace<CR>g@
" " }}}
" " Wrapped-Line Navigations {{{
vnoremap <C-j> gj
vnoremap <C-k> gk
vnoremap <C-4> g$
vnoremap <C-6> g^
vnoremap <C-0> g^

nnoremap <C-j> gj
nnoremap <C-k> gk
nnoremap <C-4> g$
nnoremap <C-6> g^
nnoremap <C-0> g^
" " }}}
" " Copy to clipboard {{{
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y
" " }}}
" " Paste from clipboard {{{
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P
" " }}}
" " Insert Mode {{{
inoremap <C-F> <c-g>u<Esc>[s1z=`]a<c-g>u
inoremap <C-U> <Esc>A
inoremap <C-Y> <Esc>ea
inoremap <C-A> <Nop>
" " }}}
" " Language-specific bindings {{{
" " ' C {{{
augroup c_bindings
    autocmd!
    autocmd Filetype c    let g:c_compiler = "clang"
    autocmd Filetype cpp  let g:c_compiler = "clang++"
    autocmd Filetype cuda let g:c_compiler = "nvcc"
    autocmd Filetype c,cpp,cuda call SetupCCompilation()
augroup end

function! SetupCCompilation()
    nnoremap <buffer> <leader>c :call CCompileCurrentFile()<CR>|
    nnoremap <buffer> <leader>r :call CRunCurrentFile()<CR>|
endfunction

" " ' }}}
" " ' Python {{{
augroup python_bindings
    autocmd!
    autocmd Filetype python let g:python_interpreter = "python3"
    autocmd Filetype python
        \ nnoremap <buffer> <leader>r :call PythonRunCurrentFile()<CR>|
        \ nnoremap <buffer> <leader>t :execute '!ctags **/*.py'<CR><CR>
augroup end
" " ' }}}
" " ' Markdown {{{
function! MarkdownBindings()
    let l:command = 's:\v(,\s*(and\|or)?|and|or)\s*:\r-   :<CR>'
    nnoremap K 1z=
    nnoremap <buffer> <leader>c :call MarkdownToPdf()<CR>
    nnoremap <buffer> <leader>r :call OpenPdf()<CR>
    nnoremap <buffer> <leader>j
        \ @=':s:\(,\s*\(and\\|or\)\?\\|\<and\\|\<or\)\s*:\r-   <C-V><CR>'<CR>
    nnoremap <buffer> <leader>b "byi>:!qutebrowser <C-R>b<CR>
    nnoremap <buffer> <leader>hy :call MarkdownCopyRichTextBuffer()<CR>
    nnoremap <buffer> <leader>hp :call MarkdownConvertClipboard()<CR>"+p
    nnoremap <buffer> <leader>hP :call MarkdownConvertClipboard()<CR>"+P
endfunction

augroup markdown_bindings
    autocmd!
    autocmd Filetype pmarkdown,markdown call MarkdownBindings()
augroup end
" " ' }}}
" " ' YAML {{{
augroup yaml_bindings
    autocmd!
    autocmd Filetype yaml nnoremap <buffer> <leader>b "byi":!qutebrowser '<C-R>b' &<CR>
augroup end
" " ' }}}
" " }}}
" " Toggles {{{
" Toggle search highlighting
nnoremap <leader>hh :set hlsearch!<CR>
" Toggle spellchecking
nnoremap <leader>s :set spell!<CR>
" Toggle trailing-whitespace highlight
nnoremap <leader>tw :call ToggleWhiteSpaceHl()<CR>
" Toggle wrap
nnoremap <leader>w :set wrap!<CR>
" " }}}
" " Other {{{
" Yank to line end
nnoremap Y y$
" Insert at first character of line
nnoremap g<C-I> ^Wi
" Remove trailing whitespace
nnoremap <F5> :.,$s:\v\s+$::c<CR><C-O>
" Make
nnoremap <leader>m :write<CR>:make<CR>
" Run file
nnoremap <leader>r :!./%<CR>
" " }}}
" }}}

" Filetype Options {{{
" " Most files {{{
augroup au_most_files
    autocmd!
    autocmd Filetype *
        \ setlocal
        \ tabstop=8
        \ softtabstop=4
        \ shiftwidth=4
        \ expandtab
        \ autoindent
        \ encoding=utf-8
        \ conceallevel=2

    autocmd Filetype * let @j="\<Esc>080lBi\<CR>\<Esc>"
    autocmd Filetype * setlocal foldmethod=syntax
augroup end
" " }}}
" " Markdown {{{

" \/ underscores should not be treated as part of the adjoining words.
augroup au_markdown
    autocmd!
    autocmd Filetype pmarkdown,yaml
        \ setlocal
        \ linebreak
        \ shiftwidth=4
        \ softtabstop=4
        \ breakindent
        \ breakindentopt=shift:4,min:20
        \ nrformats+=alpha
        \ iskeyword-=_
        \ iskeyword-=$

    autocmd Filetype markdown
        \ setlocal
        \ textwidth=79
        \ spell
        \ linebreak
        \ shiftwidth=2
        \ softtabstop=2
        \ conceallevel=0
        \ breakindent
        \ breakindentopt=shift:2,min:20
        \ matchpairs-=<:>

    autocmd Filetype pmarkdown autocmd OptionSet textwidth
                \ if (v:option_new > 0 && v:option_type == "local") | setlocal formatoptions+=an | endif

    function! PMarkdownSettings() abort
        let b:surround_43 = "**\r**"    " surround with ** instead of +
        let b:surround_33 = "~~\r~~"    " surround with ~~ instead of !
        let @h='s=":s:\`::ge:s:\([.0-9]\+\)^\([-.0-9]\+\):pow(\1,\2):ge'
    endfunction

    autocmd Filetype markdown,pmarkdown call PMarkdownSettings()
augroup end
" " }}

" " C {{{
let c_no_curly_error=1
augroup au_c
    autocmd!
    autocmd Filetype c setlocal previewheight=3 completeopt+=menuone
augroup end
" " }}}

" " C++ {{{
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
" " }}}

" " Vim {{{
augroup au_vim
    autocmd!
    autocmd Filetype vim setlocal foldmethod=marker
augroup end
" " }}}

" " Latex {{{
" let g:tex_conceal = "abdgms"
let g:vimtex_matchparen_enabled = 0
let g:vimtex_compiler_latexmk_engines = {
    \ '_'                : '-lualatex',
    \ 'pdflatex'         : '-pdf',
    \ 'dvipdfex'         : '-pdfdvi',
    \ 'lualatex'         : '-lualatex',
    \ 'xelatex'          : '-xelatex',
    \ 'context (pdftex)' : '-pdf -pdflatex=texexec',
    \ 'context (luatex)' : '-pdf -pdflatex=context',
    \ 'context (xetex)'  : '-pdf -pdflatex=''texexec --xtx''',
    \}

augroup au_latex
    autocmd!
    autocmd Filetype tex
        \ setlocal
        \ softtabstop=2
        \ shiftwidth=2
        \ linebreak
        \ breakindent
        \ breakindentopt=shift:2
augroup end
" }}}
" Sign-Column {{{
augroup au_sign_column
    autocmd!
    autocmd Filetype cpp,python setlocal signcolumn=yes
augroup end
" }}}
" }}}
" " Text files {{{
augroup au_text
    autocmd!
    autocmd Filetype text
        \ setlocal linebreak
        \ noexpandtab
        \ textwidth=0
augroup end
" " }}}
" " Shell {{{
augroup au_shell
    autocmd!
    autocmd Filetype sh setlocal noexpandtab softtabstop=0 shiftwidth=0
augroup end
" " }}}
" " No-expand tab {{{
augroup au_no_expand_tab
    autocmd!
    autocmd BufNewFile,BufRead Makefile,*.asm setlocal noexpandtab shiftwidth=0
augroup end
" " }}}
" " Two-space indent {{{
augroup au_two_space_indent
    autocmd!
    autocmd Filetype html,css,nix,coq,haskell,cpp,verilog_systemverilog
        \ setlocal softtabstop=2 shiftwidth=2
augroup end
" }}}
" }}}

set secure
