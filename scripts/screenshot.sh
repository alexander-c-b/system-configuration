#!/bin/sh
maim -s --hidecursor \
	| convert png:- $([ trim = "$1" ] && printf %s -trim) png:- \
	| to-clipboard image/png
