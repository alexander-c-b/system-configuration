#!/usr/bin/env python3
import random
import re
import sys

# TODO: add command-line switches

WORD_REGEX = r" ?[A-Za-z]+$"

HELP = f"""\
Usage: {sys.argv[0]} [replacements]
Or:    {sys.argv[0]} --help

This program reads from standard input, replacing spaces with characters
randomly chosen from `replacements` or the string ",.+/_ 0123456789".
"""

if len(sys.argv) == 2 and sys.argv[1] == "--help":
    print(HELP)
    sys.exit(1)

text = sys.stdin.read().strip()

# Argument Processing
replacements = sys.argv[1] if len(sys.argv) == 2 else ",.+/_ 0123456789"

# Main substitution
sys.stdout.write(re.sub(" ", lambda _: random.choice(replacements), text))
